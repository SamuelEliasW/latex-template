% Samuel Elias Weißenbacher (Schmu), 2023, Template for Software Development

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SoftwareTemplate}[2023/12/30 Software Template]
\LoadClass[10pt,a4paper]{article}

% Required Packages
\RequirePackage[english, german]{babel}               % include language support
\RequirePackage[utf8]{inputenc}                       % specify input encoding
\RequirePackage[dvipsnames]{xcolor}                   % enables color usage
\RequirePackage[colorlinks,linkcolor=black]{hyperref} % include http-link support
\RequirePackage{fancyvrb}                             % enhances handeling of verbatim
\RequirePackage{pdfpages}                             % include pdf support
\RequirePackage{graphicx}                             % include graphics
\RequirePackage{listings}                             % enable listings
\RequirePackage{courier}                              % default font
\RequirePackage[T1]{fontenc}                          % choose font encoding
\RequirePackage{scrhack}                              % prevent problems with listings toc
\RequirePackage{wrapfig}                              % wraping image to fit on right/left side
\RequirePackage{xstring}                              % string manipulation
\RequirePackage{subcaption}                           % for subfigures environments
\RequirePackage{fancyhdr, lastpage}                   % for header and footer and to get last page
\RequirePackage{titlesec}                             % changing sections

% Custom colors
\definecolor{sectioncolor}{RGB}{57,36,103}
\definecolor{subsectioncolor}{RGB}{93,53,135}
\definecolor{subsubsectioncolor}{RGB}{163,103,177}
\definecolor{myComment}{HTML}{227722}
\definecolor{myString}{HTML}{aa2222}
\definecolor{myKeyword}{HTML}{2222aa}
\definecolor{myLineNumber}{HTML}{666666}

%redefine footer
\fancyhf{} % clear all footer/header fields
\pagestyle{fancy}
\fancyfoot[L]{Samuel Weißenbacher}
\fancyfoot[R]{Seite \thepage/\pageref{LastPage}}

% Format section titles
\titleformat{\section}
  {\color{sectioncolor}\Large\bfseries}
  {\thesection}
  {1em}
  {}
  [\titlerule] % Add a horizontal line

% Format subsection titles
\titleformat{\subsection}
  {\color{subsectioncolor}\large\bfseries}
  {\thesubsection}
  {1em}
  {}
  [\titlerule] % Add a horizontal line

  
% Format subsubsection titles
\titleformat{\subsubsection}
   {\color{subsectioncolor}\large\bfseries}
   {\thesubsubsection}
   {1em}
   {}
   [\titlerule] % Add a horizontal line

% Add style for verbatim
\lstdefinestyle{text}
{
   language=text,
   basicstyle=\ttfamily\small,
   basewidth=\basewidth,
   breakindent=4\basewidth,
   postbreak=\llap{$\hookrightarrow$}
}

\lstloadlanguages{C++, Java} % Load syntax for listings, for a list of other languages supported see: ftp://ftp.tex.ac.uk/tex-archive/macros/latex/contrib/listings/listings.pdf

\lstset{%
   basicstyle=\footnotesize\ttfamily,        % set basic style of code
   showspaces=false,showstringspaces=false,  % dont show string spaces in code
   numbers=left,tabsize=3,breaklines,        % place line number
   breaklines=true,                          % automatic line breaking only at whitespace
   captionpos=b,                             % sets the caption-position to bottom
   commentstyle=\color{myComment},           % comment style
   keywordstyle=\bfseries\color{myKeyword},  % keyword style
   stringstyle=\color{myString},             % string literal style
   numberstyle=\color{myLineNumber},		   % linenumber style
   identifierstyle=\color{black},            % style for all identifiers
   literate=                                 % support non ascii char
      {ö}{{\"o}}1
      {ä}{{\"a}}1
      {ü}{{\"u}}1
      {Ö}{{\"O}}1
      {Ä}{{\"A}}1
      {Ü}{{\"U}}1
      {ß}{{\ss}}1
      {_}{{\_}}1
      {°}{{$^{\circ}$}}1
}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1} % Line spacing

\setlength\parskip{0.3\baselineskip}
\setlength\parindent{0pt} % Removes all indentation from paragraphs

\lstdefinestyle{C}{
   language=C
}

\lstdefinestyle{Text}{
   numbers=none
}

\lstdefinestyle{C++}{
   language=C++
}

\lstdefinestyle{Java}{
   language=Java
}

\lstdefinestyle{CSharp}{
   language=C++,
   morekeywords={input,output,attribute,uint}
}

\lstdefinestyle{Pseudocode}{
   language=C,
   morekeywords={algorithm,param,local,array,end,then,to,by},
   literate=
      {[in]}{$\downarrow$}1
      {[inout]}{$\updownarrow$}1
      {[out]}{$\uparrow$}1
      {!=}{$\neq$}1
}

%include tilepage
% 1 = title
% 2 = subtitel
\newcommand{\TitlePage}[2]{
\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	
	\raggedleft % Right align the title page

	\rule{1pt}{\textheight} % Vertical line
	\rule{4pt}{\textheight} % Vertical line
	\hspace{0.05\textwidth} % Whitespace between the vertical line and title page text
	\parbox[b]{0.75\textwidth}{ % Paragraph box for holding the title page text, adjust the width to move the title page left or right on the page
		
		{\Huge\bfseries #1}\\[2\baselineskip] % Title
		{\large\textit{\underline{#2}}}\\[4\baselineskip] % Subtitle or further description
		{\Large\textsc{samuel weißenbacher}} % Author name, lower case for consistent small caps
		
		\vspace{0.5\textheight} % Whitespace between the title block and the publisher
		
		\today
	}
	\rule{4pt}{\textheight} % Vertical line
	\rule{1pt}{\textheight} % Vertical line

\end{titlepage}
}

%include file with certain style
% 1 = filename
% 2 = style
% 3 = path
% 4 = caption
\newcommand{\IncFile}[4]{
   % check if filename contains path
   \IfSubStr{#1}{/}{
      % Extract filename
      \StrBehind*{#1}{/}[\filename]
      \StrBehind*{#4}{/}[\filedescription]
      \IncFile{\filename}{#2}{#3}{\filedescription}
   }{
      % #1 equals only the filename
      %\typeout{Name generated: #1}
      \begin{itemize}
         \item[]\lstinputlisting[style=#2, caption=#4, label=#4]{#3}
      \end{itemize}
   }
}

\newcommand{\SourceFile}[2]{
   \StrSubstitute{#1}{_}{-}[\safeFileName]
   \IncFile{#1}{#2}{#1}{\safeFileName}
}

\newcommand{\CFile}[1]{
   \SourceFile{#1}{C}
}

\newcommand{\CSharpFile}[1]{
   \SourceFile{#1}{CSharp}
}

\newcommand{\CppFile}[1]{
   \SourceFile{#1}{C++}
}

\newcommand{\JavaFile}[1]{
   \SourceFile{#1}{Java}
}

\newcommand{\CModule}[1]{
   \SourceFile{#1.h}{C}
   \SourceFile{#1.c}{C}
}

\newcommand{\CppModule}[1]{
   \SourceFile{#1.h}{C++}
   \SourceFile{#1.cpp}{C++}
}

%include a text file by verbatim
% 1 = filename
\newcommand{\TextFile}[1]{
   \SourceFile{#1}{Text}
}

%include pdf
% 1 = filename
% 2 = style
\newcommand{\IncPdf}[2]{
   \includepdf[fitpaper,scale=1, pages=1, pagecommand={\thispagestyle{empty}{#2}}]{#1}
   \pdfximage{#1}
   \ifthenelse{\equal{\the\pdflastximagepages}{1}}
   {} %true
   {\includepdf[fitpaper, pages=2-last, pagecommand={\thispagestyle{empty}}]{#1}}
   \pagebreak
}

% print list of all sources
\newcommand{\ListSources}{
   \lstlistoflistings
	\pagebreak
}

%include centered image
% 1 = filename
% 2 = caption
% 3 = size of image
\newcommand{\Image}[3]{
    \begin{figure}[!h]
        \centering
        \includegraphics[width=#3\linewidth]{#1}
        \caption{#2}
    \end{figure}
}

%include right aligned image
% 1 = filename
% 2 = caption
% 3 = size of image
\newcommand{\RightImage}[3]{   
   \begin{wrapfigure}{r}{#3\textwidth} %this figure will be at the right
      \centering
      \includegraphics[width=#3\textwidth]{#1}
      \caption{#2}
   \end{wrapfigure}
}

%include left aligned image
% 1 = filename
% 2 = caption
% 3 = size of image
\newcommand{\LeftImage}[3]{   
   \begin{wrapfigure}{l}{#3\textwidth} %this figure will be at the right
      \centering
      \includegraphics[width=#3\textwidth]{#1}
      \caption{#2}
   \end{wrapfigure}
}

%include two images side by side
% 1 = filename       1
% 2 = caption        1
% 3 = size of image  1
% 4 = filename       2
% 5 = caption        2
% 6 = size of image  2
\newcommand{\TwoImages}[6]{
   \begin{figure}[!h]
      \centering
      \begin{minipage}{0.5\textwidth}
         \centering
         \includegraphics[width = #3\textwidth]{#1}
         \captionof{figure}{#2}
      \end{minipage}%
      \begin{minipage}{0.5\textwidth}
         \centering
         \includegraphics[width = #6\textwidth]{#4}
         \captionof{figure}{#5}
      \end{minipage}
   \end{figure}
}

% Create a table
% 1 -> First Row
% 2 -> Other Rows
% 3 -> Caption
\newcommand{\NewTable}[3]{
   %extract format from first column
   \StrCount{#1}{&}[\thenumamps]
   \edef\columnSize{}
   \foreach \x in {1,...,\thenumamps}{
      \xdef\columnSize{\columnSize | c }
   }
   % add last occurence
   \xdef\columnSize{\columnSize | c |}

   \begin{table}[h!]
      \centering
      \begin{tabular}{\columnSize}
         \hline 
         #1 \\
         \hline
         #2 \\
         \hline
      \end{tabular}
      \caption{#3}
   \end{table}
}
