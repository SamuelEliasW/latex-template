# LaTeX Template Summary

## Purpose
This LaTeX template, created by Samuel Elias Weißenbacher (Schmu) in April 2024, serves as a framework for software development documentation. It facilitates the inclusion of PDFs, C, C++, and text files, along with the provision for displaying code snippets in various programming languages.

## Features
- **Inclusion of Various File Types**: The template supports the inclusion of PDFs, C, C++, and text files, enabling the integration of diverse content into the document.
- **Path Exclusion in Code Files**: Paths in code files are automatically excluded, simplifying the presentation and making file references cleaner.

## Usage
To utilize this template effectively:
1. Ensure all required LaTeX packages are installed.
2. Use the provided commands (`\CFile`, `\CppFile`, `\JavaFile`, `\TextFile`, `\IncPdf`, etc.) to include respective file types into the document.
3. Make use of the provided commands for inserting images (`\Image`, `\RightImage`, `\LeftImage`, `\TwoImages`) to enhance visual content.
4. Utilize `\ListSources` to print a list of all included source files in the document.

## Example Usage
```latex
\documentclass{SoftwareTemplate}

\begin{document}

\TitlePage{Document Title}{Subtitle}

\section{Introduction}
Lorem ipsum dolor sit amet, consectetur adipiscing elit. 

\CFile{example.c}

\section{Analysis}
\subsection{Subsection}
Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

\JavaFile{Example.java}

\ListSources

\end{document}
```

## Notes
- This template simplifies the creation of software development documentation by providing structured layouts and convenient commands for including various file types.
- It promotes consistency and cleanliness in document formatting and presentation.